package com.contratante.biro;

public class Empresa {

    String cnpj;

    String statusAvaliacao;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getStatusAvaliacao() {
        return statusAvaliacao;
    }

    public void setStatusAvaliacao(String statusAvaliacao) {
        this.statusAvaliacao = statusAvaliacao;
    }
}
