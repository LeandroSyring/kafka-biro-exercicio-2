package com.contratante.biro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class BiroApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiroApplication.class, args);
	}

}
