package com.contratante.biro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.KafkaListeners;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {

    @Autowired
    private EmpresaService empresaService;

    @KafkaListener(topics = "spec2-leandro-guilherme-2", groupId = "grupo-1")
    public void receber(@Payload Empresa empresa){

        System.out.println(empresa.getCnpj());

        empresaService.validarCapitalEmpresa(empresa);
    }
}
