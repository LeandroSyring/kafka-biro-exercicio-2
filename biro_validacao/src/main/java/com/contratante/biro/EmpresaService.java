package com.contratante.biro;

import com.contratante.biro.client.EmpresaGovernoClient;
import com.contratante.biro.client.EmpresaGovernoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    private EmpresaGovernoClient empresaGovernoClient;

    @Autowired
    private EmpresaProducer empresaProducer;

    public void validarCapitalEmpresa(Empresa empresa){

        EmpresaGovernoDTO empresaGovernoDTO = empresaGovernoClient.getEmpresaGoverno(empresa.getCnpj());

        if(empresaGovernoDTO.getCapital_social() >= 1000000){
            empresa.setStatusAvaliacao("Aceita");
        }
        else{
            empresa.setStatusAvaliacao("Recusada");
        }

        empresaProducer.enviarAoKafka(empresa);
    }
}
