package com.contratante.biro.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "empresa", url = "https://www.receitaws.com.br/v1/cnpj/")
public interface EmpresaGovernoClient {

    @RequestMapping("/{cnpj}")
    EmpresaGovernoDTO getEmpresaGoverno(@PathVariable String cnpj);
}
