package com.contratante.biro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    private EmpresaProducer empresaProducer;

    @PostMapping
    public void enviarEmpresa(@RequestBody EmpresaRequest empresaRequest){

        Empresa empresa = new Empresa();
        empresa.setCnpj(empresaRequest.getCnpj());
        empresa.setStatusAvaliacao("Não avaliada");
        empresaProducer.enviarAoKafka(empresa);
    }
}
