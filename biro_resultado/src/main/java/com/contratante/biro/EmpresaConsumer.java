package com.contratante.biro;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class EmpresaConsumer {

    @KafkaListener(topics = "spec2-leandro-guilherme-3", groupId = "grupo-1")
    public void receber(@Payload Empresa empresa) throws IOException {

        System.out.println("CNPJ: " + empresa.getCnpj() + " - Status Avaliação: " + empresa.getStatusAvaliacao() + ".");

        FileWriter arq = new FileWriter("cadastros.txt", true);
        PrintWriter gravarArq = new PrintWriter(arq);

        gravarArq.println(empresa.getCnpj() + ";" + empresa.getStatusAvaliacao());

        arq.close();
    }
}
